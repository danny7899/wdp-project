# Web Design and Programming Project

[![pipeline status](https://gitlab.com/danny7899/wdp-project/badges/master/pipeline.svg)](https://gitlab.com/danny7899/wdp-project/commits/master)
[![coverage report](https://gitlab.com/danny7899/wdp-project/badges/master/coverage.svg)](https://gitlab.com/danny7899/wdp-project/commits/master)

## Members:
- Aufa Wiandra Moenzil - 1706067512
- Danny August Ramaputra - 1706019791
- Micahel Sudirman - 1706020300
- Riza Kusuma - 1706019412

## Heroku app link:
http://study-room.herokuapp.com/